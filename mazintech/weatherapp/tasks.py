import os
import csv
import json
import xmltodict
from lib.excel_helper import ExcelHelper
from lib.file_handler import file_download, save_file
from datetime import datetime, timedelta
from django.utils import timezone
from django.conf import settings
from django.core.files import File
from django.http import HttpResponse
from mazintech.celery import app
from celery.contrib import rdb
from celery.schedules import crontab
from models import *
from datetime import datetime
from django.db import transaction
import sys
from django.db.models import Sum, Count
from django.db.models import Q, F, Case, When, FloatField, IntegerField, Value
from django.http import JsonResponse


reload(sys)
sys.setdefaultencoding('utf8')

import logging
logger = logging.getLogger(__name__)

@app.task(name='test')
def test():
    print 'celery beat is working'


@app.task(name='save_weather_data')
def save_weather_data(request):      
    response = requests.post(
                            'http://weather.news24.com/ajaxpro/Weather.Code.Ajax,Weather.ashx',
                            json={'cityId': '77107'},
                            headers={'Content-Type': 'text/plain', 
                                     'X-AjaxPro-Method': 'GetCurrentOne'},
                        )
    json_response = response.json()
    repository = json_response
    value = repository['value']
    location = value['Location']
    forecasts = location['Forecasts']
    for forecast in forecasts:
        weather = Weather.objects.create(weather_date=datetime.now(),
                          min_temp=forecast['LowTemp'],
                          max_temp=forecast['HighTemp'],
                          wind=forecast['WindSpeed'],
                          rain=forecast['Rainfall'])

    return return JsonResponse(forecasts)



    