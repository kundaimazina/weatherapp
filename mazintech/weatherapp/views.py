import os
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.db.models import Count, Sum, Q, F, ExpressionWrapper, IntegerField
from django.db.models.functions import Concat
from django.db.models import Value as V
import json
import requests
from requests.auth import HTTPBasicAuth
import threading

from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import View
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User

from models import *
from forms import *
from exporter import *
from mazintech.models import *
from mazintech.forms import AddressForm, ContactForm
from dal import autocomplete
from datetime import datetime


@login_required
def weather_list(request, template="weather/weather.html"):
    context = {}
    filter_form = WeatherFilterForm(request.GET or None)
        
    weather_list = Weather.objects.all()
   

    if u'search' in request.GET :
        if filter_form.is_valid():
            weather_list = filter_form.filter(weather_list)

    context['weather_list'] = weather_list
    context['weather_filter_form'] = filter_form

    return render(request, template, context)


def save_weather_data(request):
    try:     
        response = requests.post(
                                'http://weather.news24.com/ajaxpro/Weather.Code.Ajax,Weather.ashx',
                                json={'cityId': '77107'},
                                headers={'Content-Type': 'text/plain', 
                                         'X-AjaxPro-Method': 'GetCurrentOne'},
                            )
        json_response = response.json()
        repository = json_response
        value = repository['value']
        location = value['Location']
        forecasts = location['Forecasts']
        for forecast in forecasts:
            weather = Weather.objects.create(weather_date=datetime.now(),
                              min_temp=forecast['LowTemp'],
                              max_temp=forecast['HighTemp'],
                              wind=forecast['WindSpeed'],
                              rain=forecast['Rainfall'])
    except Exception, ex:
        logger.exception(ex)
        messages.error(request, "Failed while processing export. Error was %s" % (ex))

    return redirect(reverse('home'))
