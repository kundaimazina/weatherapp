from django.db import models
from lib.models import BaseModel
from lib.fields import ProtectedForeignKey
from dal import autocomplete
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

from django.contrib.auth.models import AbstractUser

class MazintechUser(AbstractUser):
    phone_number = models.CharField(null=True, max_length=50)
    signature = models.FileField(null=True, blank=True, upload_to='uploads/accounts')
    regional_staff = models.BooleanField(blank=True, default=False)
    region = ProtectedForeignKey('mazintech.Region', null = True, blank = True, related_name='user_region')
    district = ProtectedForeignKey('mazintech.Branch', null = True, blank = True, related_name='user_district')

    def __unicode__(self):
        return self.username


    @property
    def full_name(self):
        full_name = "{} {}".format(self.first_name,self.last_name) \
                    if self.first_name and self.last_name else \
                    "{}".format(self.first_name) if self.first_name \
                    else "{}".format(self.last_name) if self.last_name \
                    else None
        return full_name

    @property
    def can_edit(self):
        return self.is_superuser or self.privilege == 'create_elopsysuser' or self.privilege == 'admin'

    @property
    def is_executive(self):
        return 'mazintech Executive' in self.groups.all()


    @property
    def is_mazintech_employee(self):
        return 'Mazintech Employee' in self.groups.all().values_list('name', flat=True)


    @property
    def is_administrator(self):
        return 'Administrator' in self.groups.all().values_list('name', flat=True)

    @property
    def is_management(self):
        return 'Management' in self.groups.all().values_list('name', flat=True)

    @property
    def is_regional_manager(self):
        return 'Regional Manager' in self.groups.all().values_list('name', flat=True)

    @property
    def is_regional_user(self):
        return self.regional_staff

    @property
    def get_user_region(self):
        if self.region is None:
            return Region.objects.none()
        else:
            return self.region.id

    @property
    def get_user_district(self):
        if self.district is None:
            return Branch.objects.none()
        else:
            return self.district.id

class Contact(BaseModel):
    first_name = models.CharField(max_length=255, null=False, blank=False, verbose_name='Contact First Name')
    last_name = models.CharField(max_length=255, null=True, blank=True, verbose_name='Contact Last Name')
    tel_number = models.CharField(max_length=255, null=True, blank=True, verbose_name='Telephone Number')
    tel_number_2 = models.CharField(max_length=255, null=True, blank=True, verbose_name='Telephone Number 2')
    tel_number_3 = models.CharField(max_length=255, null=True, blank=True, verbose_name='Telephone Number 3')
    cell_number = models.CharField(max_length=255, null=True, blank=True, db_index=True, verbose_name='Cell Number')
    email = models.CharField(max_length=255, null=True, blank=True, db_index=True)

class Address(BaseModel):

    ADDRESS_TYPES = (('postal','Postal'),('residential','Residential'),
                     ('business','Business'),('residential', 'Residential'),
                     ('pobox','POBox'))
    
    address_type = models.CharField(max_length=50, null=False, blank=False,
                                    choices=ADDRESS_TYPES, db_index=True)
    address_line_1 = models.CharField(max_length=255, null=False, blank=False, verbose_name='Address Line 1')
    address_line_2 = models.CharField(max_length=255, null=True, blank=True, verbose_name='Address Line 2')
    suburb = models.CharField(max_length=255, null=True, blank=True)
    postal_code = models.CharField(max_length=255, null=True, blank=True, verbose_name='Postal Code')
    city = models.CharField(max_length=255, null=False, blank=False, db_index=True)
    province = models.CharField(max_length=255, null=True, blank=True)
    country = models.CharField(max_length=255, null=False, blank=False,
                               default="South Africa")

class ThumbnailImageField(ImageSpecField):
    def __init__(self, *args, **kwargs):
        super(ThumbnailImageField, self).__init__(processors=[ResizeToFill(400, 400)],
                                                  options={'quality': 80},
                                                  *args, **kwargs)
        
class Document(BaseModel):

    FILE_TYPES = (('document','Document'),('photo','Photo'))
    
    document_name = models.CharField(max_length=255, null=False, blank=False, db_index=True)
    
    document = models.FileField(null=True, blank=True, upload_to='uploads/documents')
    image = models.ImageField(null=True, blank=True, upload_to="uploads/images")
    thumbnail = ThumbnailImageField(source="image")
    
    file_type = models.CharField(max_length=50, null=False, blank=False,
                                 choices=FILE_TYPES, db_index=True)
    
    description = models.TextField(null=True, blank=True)
    created_by = ProtectedForeignKey('mazintech.MazintechUser', null=True, blank=True,
                                     db_index=True)

class Comment(BaseModel):
    comment = models.TextField(null=True, blank=True)
    created_by = ProtectedForeignKey('mazintech.MazintechUser', null=True, blank=True,
                                     db_index=True)

class Region(BaseModel):
    code = models.CharField(max_length=255, unique=True, null=False, blank=False)
    name = models.CharField(max_length=255, unique=True, null=False, blank=False)

    def __unicode__(self):
        return "{} - {}".format(self.code, self.name)

    @property
    def region_name(self):
        return "{} - {}".format(self.code, self.name)
    
class Branch(BaseModel):

    OFFICE_TYPES = (('Head Office', 'Head Office'),
                    ('Regional', 'Regional'),
                    ('Satelite', 'Satelite'))
    code = models.CharField(max_length=255, unique=True, null=False, blank=False)
    description = models.CharField(max_length=255, null=False, blank=False)
    office_type = models.CharField(max_length=50, null=True, blank=True, default='new',
                                   choices=OFFICE_TYPES, db_index=True)
    region = ProtectedForeignKey('mazintech.Region', null=True, blank=True, related_name='branch_region')
    contact_person = ProtectedForeignKey('mazintech.Contact', null=True, blank=True, related_name='branch_contact')
    address = ProtectedForeignKey('mazintech.Address', null=True, blank=True, related_name='branch_address')
    
    def __unicode__(self):
        return "{} - {}".format(self.code, self.description)

    @property
    def branch_name(self):
        return "{} - {}".format(self.code, self.description)

class RegionAutoComplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Region.objects.none()

        qs = Region.objects.all()

        if self.q:
            qs = qs.filter(Q(name__icontains=self.q) |
                           Q(code__icontains=self.q)).order_by('name')
        return qs

class DistrictAutoComplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Branch.objects.none()

        region_id = self.forwarded.get('region', None)
        qs = Branch.objects

        if region_id:
            qs = qs.filter(region__pk=region_id)
        else:
            return Branch.objects.none()

        if self.q:
            qs = qs.filter(Q(name__icontains=self.q) |
                           Q(code__icontains=self.q)).order_by('name')
        return qs
