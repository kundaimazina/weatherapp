from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.contrib import admin
from dal import autocomplete

import views
# from mazintech.autocomplete import *

from .models import *
from mazintech.models import *

urlpatterns = [
    url(r'^weather_list/$', views.weather_list, name='weather_list'),
    url(r'^weather_data/$', views.save_weather_data, name='weather_data'),    
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


