# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db import models
from lib.models import BaseModel
from lib.fields import ProtectedForeignKey
from dal import autocomplete
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill
# Create your models here.


class Weather(BaseModel):
    weather_date = models.CharField(max_length=255, null=False, blank=False, verbose_name='Date')
    min_temp = models.CharField(max_length=255, null=True, blank=True, verbose_name='Min Temperature')
    max_temp = models.CharField(max_length=255, null=True, blank=True, verbose_name='Max Temperature')
    wind = models.CharField(max_length=255, null=True, blank=True, verbose_name='Wind Speed')
    rain = models.CharField(max_length=255, null=True, blank=True, verbose_name='Rainfall')