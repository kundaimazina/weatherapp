from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from . import views
from django.contrib.auth import views as auth_views
from django_js_reverse.views import urls_js
from models import RegionAutoComplete,DistrictAutoComplete

urlpatterns = [
    url(r'^$', views.home, name='home'),

    url(r'^region_autocomplete/$', RegionAutoComplete.as_view(), name='region_autocomplete'),
    url(r'^district_autocomplete/$', DistrictAutoComplete.as_view(), name='district_autocomplete'),

    url(r'^jsreverse/$', urls_js, name='js_reverse'),
    url(r'^accounts/login/$', auth_views.login, name='login'),
    url(r'^accounts/signup/$', views.signup, name='signup'),
    url(r'^accounts/logout/$', auth_views.logout,{'next_page':'/'}, name='logout'),
    url(r'^about/$', views.about, name='about'),
    url(r'^users$', views.users, name='users'),
    url(r'^users/college-admins/(?P<college_administrators>\d+)/$', views.users, name='college_admins'),
    url(r'^user/add/$', views.user_edit, name='user_add'),
    url(r'^user/edit/(?P<user_id>\d+)/$', views.user_edit, name='user_edit'),

    url(r'^', include('weatherapp.urls', namespace='weatherapp')),

    url(r'^regions$', views.view_regions, name='view_regions'),
    url(r'^branches$', views.view_branches, name='view_branches'),

    url(r'^downloads/$', views.downloads_list, name="view_downloads"),
    url(r'^downloads/(?P<file_id>\d+)/$', views.file_download_task, name="file_download"),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
