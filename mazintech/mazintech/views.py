from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required,user_passes_test
from django.contrib.auth.decorators import permission_required
from django.contrib.auth import authenticate
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from models import MazintechUser, Region, Branch
from forms import MazintechUserForm, MazintechUserFilterForm, RegionForm
from forms import BranchForm, DownloadsFilterForm, SignUpForm
from mazintech.models import *
from weatherapp.models import *
from lib.file_handler import file_download
import threading
import datetime
from django.contrib import messages

@login_required
def about(request): 
    return render(request, "about.html", {})

@login_required
def home(request, template='mazintech/home.html'):

    context = {}
    weather_now = Weather.objects.all().order_by('-weather_date').first()
    weather_list = Weather.objects.all().order_by('-weather_date')
    context['weather_now'] = weather_now
    context['weather_list'] = weather_list
    
    return render(request, template, context)

@login_required
@user_passes_test(lambda u: u.is_superuser) 
def users(request, college_administrators=False, template="users/users.html"):
    
    context = {}
    user_filter_form = MazintechUserFilterForm(request.POST or None)
    if college_administrators:
        users = MazintechUser.objects.filter(groups__name='College Administrators')
        title = "College Administrators"
    else:
        users = MazintechUser.objects.all()
        title = "Users"

    if not request.user.is_superuser:
        return redirect(reverse('logout'))

    if u'search' in request.POST:
        if user_filter_form.is_valid():
            users = user_filter_form.filter(users)
            if len(set(user_filter_form.cleaned_data.values())) > 1:
                context['reset_button'] = True

    
    context['users'] = users
    context['title'] = title
    context['user_filter_form'] = user_filter_form
    return render(request, template, context)

@login_required
@user_passes_test(lambda u: u.is_superuser) 
def user_edit(request, user_id=None, template="users/user_edit.html"):
    context = {}
    user = MazintechUser.objects.get(pk=user_id) if user_id else None
    user_form = MazintechUserForm(request.POST or None, instance=user)

    if not request.user.is_superuser:
        return redirect(reverse('logout'))

    if u'save' in request.POST  and  user_form.is_valid():
        user = user_form.save()
        next = request.POST.get('next', '/')
            
        return HttpResponseRedirect(next)

    if u'cancel' in request.POST:
        next = request.POST.get('next', '/')
        return HttpResponseRedirect(next)
        

    context['form'] = user_form
    context['user'] = user
    return render(request, template, context)


def signup(request, template="users/user_edit.html"):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            next = request.POST.get('next', '/')            
            return HttpResponseRedirect(next)
    else:
        form = SignUpForm()

    if u'cancel' in request.POST:
        next = request.POST.get('next', '/')
        return HttpResponseRedirect(next)
    return render(request, template, {'form': form})        


@login_required
def view_regions(request, template="mazintech/view_regions.html"):
    context = {}

    regions = Region.objects.all().order_by('code')
    
    context['regions'] = regions

    return render(request, template, context) 

@login_required
def view_branches(request, template="mazintech/view_branches.html"):
    context = {}

    branches = Branch.objects.all()
    
    context['branches'] = branches

    return render(request, template, context)


@login_required
def downloads_list(request, template="mazintech/downloads.html"):
    context = {}
   
    downloads = Document.objects.filter(file_type__in=['Download']).order_by("-created_at")
    filter_form = DownloadsFilterForm(request.POST or None)
    if u'search' in request.POST:
        if filter_form.is_valid():
            downloads = filter_form.filter(downloads)

    context['downloads'] = downloads
    context['dl_user'] = request.user
    context['filter_form'] = filter_form

    return render(request, template, context)


def file_download_task(request, file_id):
    saved_file = Document.objects.get(pk=file_id)
    return file_download(file=saved_file)