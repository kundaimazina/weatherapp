import os

from django.conf import settings
from django import forms
from django.db.models import Q
from datetime import datetime, timedelta
from django.db.models.functions import Concat
from django.db.models import Value as V
from django.forms.widgets import *
from models import *
from rest_framework import fields, serializers
from django.core.urlresolvers import reverse
from mazintech.models import *
from dal import autocomplete, forward
from django.forms.widgets import RadioSelect

class WeatherFilterForm(forms.Form):
    
    date_from = forms.DateTimeField(required=False,label='From Date')
    date_to   = forms.DateTimeField(required=False,label='To Date')

    def __init__(self, *args, **kwargs):
        super(WeatherFilterForm, self).__init__(*args, **kwargs)
        self.fields['date_from'].widget.attrs['class'] = 'date_field'
        self.fields['date_to'].widget.attrs['class'] = 'date_field'

    def filter(self, weather_list):
        if self.cleaned_data is not None:

            if self.cleaned_data['date_to']:
                weather_list = weather_list.filter(created_at__lte=self.cleaned_data['date_to'])

            if self.cleaned_data['date_from']:
                weather_list = weather_list.filter(created_at__gte=self.cleaned_data['date_from'])

        return weather_list
