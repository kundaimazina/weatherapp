import os

from django.conf import settings
from django import forms
from django.db.models import Q
from datetime import datetime
from django.db.models import Value as V
from django.db.models.functions import Concat
from dal import autocomplete, forward
from django.forms.widgets import *
from django.core.exceptions import ValidationError
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from models import MazintechUser, Region, Branch, Document, Address, Contact


class MazintechUserForm(forms.ModelForm):

    password1 = forms.CharField(
        required=False, label="Password (leave empty if not changed)", widget=forms.PasswordInput)
    password2 = forms.CharField(
        required=False, label="Re-enter password", widget=forms.PasswordInput)
    region = forms.ModelChoiceField(required=False,
                                    queryset=Region.objects.all(),
                                    empty_label=None,
                                    widget=autocomplete.ListSelect2(url='region_autocomplete',
                                    attrs={'data-placeholder': "---------"}
                                    ))

    district = forms.ModelChoiceField(required=False,
                                      queryset=Branch.objects.all(),
                                      empty_label=None,
                                      widget=autocomplete.ListSelect2(url='district_autocomplete',
                                      attrs={'data-placeholder': "---------"},
                                      forward=('region',)
                                      ))        
    class Meta:
        model = MazintechUser
        fields = ['first_name','last_name','username','email','phone_number',
                  'region','district','signature','groups','regional_staff',
                  'is_active']       
        widgets = {'groups': forms.CheckboxSelectMultiple()}

    def __init__(self, *args, **kwargs):
        super(MazintechUserForm, self).__init__(*args, **kwargs)
        self.fields['district'].widget.attrs['class'] = 'multi_select'
        
    def clean_password1(self):
        if not self.cleaned_data.get('password1') and not self.instance.pk:
            raise ValidationError("Please enter a password for this user")
        return self.cleaned_data.get('password1')

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 != password2:
            raise ValidationError("Passwords do not match")

        if not password1 and not self.instance.pk:
            raise ValidationError("Please enter a password for this user")

        return password2

    def save(self, commit=True, *args, **kwargs):
        ops_user = super(MazintechUserForm, self).save(commit=False)
        region = self.instance.region

        if self.cleaned_data.get('password1'):
            ops_user.set_password(self.cleaned_data.get('password1'))

        groups = self.cleaned_data.get('groups', [])
        district = self.cleaned_data.get('district', [])
        
        if commit:
            ops_user.save()
            ops_user.groups = groups

        return ops_user        


class MazintechUserFilterForm(forms.Form):

    name = forms.CharField(required = False)
    active = forms.ChoiceField(required = False, choices = (('', "--- Select User Status ---"), ('active', "Active"), ("inactive", "Inactive")))

    def __init__(self, *args, **kwargs):
        super(MazintechUserFilterForm, self).__init__(*args, **kwargs)

        self.fields['name'].widget.attrs['placeholder'] = 'User, Name,Email, Phon.... '

    def filter(self, users):
        if self.cleaned_data is not None:
            if self.cleaned_data['active']:
                if self.cleaned_data['active'] == 'active':
                    users = users.filter(is_active=True)
                if self.cleaned_data['active'] == 'inactive':
                    users = users.filter(is_active=False)


            if self.cleaned_data['name']:
                users = users.annotate(fullname=Concat('first_name', V(" "), 'last_name'))
                term = self.cleaned_data['name']
                users = users.filter(Q(first_name__icontains=term) |
                                     Q(last_name__icontains=term) |
                                     Q(fullname__icontains=term) |
                                     Q(email__icontains=term) |
                                     Q(phone_number__icontains=term) |
                                     Q(username__icontains=term))
          
            
        return users


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Last Name Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = MazintechUser
        fields = ('username', 'first_name', 'last_name', 
                  'email', 'password1', 'password2', )

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)

    def clean_password1(self):
        if not self.cleaned_data.get('password1') and not self.instance.pk:
            raise ValidationError("Please enter a password for this user")
        return self.cleaned_data.get('password1')

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 != password2:
            raise ValidationError("Passwords do not match")

        if not password1 and not self.instance.pk:
            raise ValidationError("Please enter a password for this user")

        return password2


class RegionForm(forms.ModelForm):

    class Meta:
        model = Region
        exclude = ['deleted']


class BranchForm(forms.ModelForm):

    class Meta:
        model = Branch
        exclude = ['deleted']


class DocumentFileForm(forms.ModelForm):
    description = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = Document
        fields = ['document', 'description']


class PhotoFileForm(forms.ModelForm):
    
    class Meta:
        model = Document
        fields = ['image', 'description']
        

class AddressForm(forms.ModelForm):
    
    class Meta:
        model = Address
        exclude = ['deleted', 'address_type']


class ContactForm(forms.ModelForm):

    class Meta:
        model = Contact
        fields = ['first_name', 'last_name', 'tel_number', 'cell_number', 'email']


class DownloadsFilterForm(forms.Form):
    name = forms.CharField(required = False)
    start_date = forms.CharField(required=False, label='Start date')
    end_date = forms.CharField(required=False, label='End date')

    def __init__(self, *args, **kwargs):
        super(DownloadsFilterForm, self).__init__(*args, **kwargs)

        self.fields['name'].widget.attrs['placeholder'] = 'Search Downloaded By, File Name ,Desc...'
        self.fields['end_date'].widget.attrs['class'] = 'date_field'
        self.fields['start_date'].widget.attrs['class'] = 'date_field'
        self.fields['start_date'].widget.attrs['placeholder'] = 'Start Date'
        self.fields['end_date'].widget.attrs['placeholder'] = 'End Date'
        
    def filter(self, downloads):
        if self.cleaned_data is not None:
            if self.cleaned_data['name']:
                users = MazintechUser.objects.filter(username__icontains=self.cleaned_data['name']).values_list('id', flat=True)
                                                                    
                downloads = downloads.filter(Q(description__icontains=self.cleaned_data['name'])|
                                             Q(document_name__icontains=self.cleaned_data['name'])|
                                             Q(created_by__in=users))

            if self.cleaned_data['start_date']:
                    downloads = downloads.filter(created_at__gte=self.cleaned_data['start_date'])

            if self.cleaned_data['end_date']:
                    downloads = downloads.filter(created_at__lte=self.cleaned_data['end_date'])
        
        return downloads